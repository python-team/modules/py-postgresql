py-postgresql (1.2.1+git20180803.ef7b9a9-5) UNRELEASED; urgency=medium

  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 31 Oct 2022 20:50:26 -0000

py-postgresql (1.2.1+git20180803.ef7b9a9-4) unstable; urgency=medium

  [ Stefano Rivera ]
  * Team upload.
  * Patch: Python 3.10 support. (Closes: #999367)

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.1, no changes needed.

 -- Stefano Rivera <stefanor@debian.org>  Thu, 18 Nov 2021 23:14:39 -0400

py-postgresql (1.2.1+git20180803.ef7b9a9-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Stefano Rivera ]
  * Update Maintainer to Debian Python Team.

 -- Sandro Tosi <morph@debian.org>  Tue, 24 Aug 2021 00:03:51 -0400

py-postgresql (1.2.1+git20180803.ef7b9a9-2) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs.
  * Bump Standards-Version to 4.4.1.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.
  * Fix day-of-week for changelog entries 1.0.2-1.

  [ Håvard Flaget Aasen ]
  * Add dh-python as b-d closes: #944937

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Sun, 15 Dec 2019 11:51:51 +0100

py-postgresql (1.2.1+git20180803.ef7b9a9-1) unstable; urgency=medium

  * New upstream release
  * d/control:
    - Change Maintainer to DPMT
    - Change Vcs-* tags to DPMT
    - Remove X-Python3-Version, no longer needed
  * d/README.Debian: upstream has tagged 1.2.1 in git, remove that file

 -- William Grzybowski <william@grzy.org>  Wed, 18 Sep 2019 09:48:31 -0300

py-postgresql (1.1.0+git20180802.ef7b9a9-1) unstable; urgency=medium

  * New upstream release.
  * Add Salsa CI support.
  * Run wrap-and-sort.
  * debian/compat: remove, no longer needed for 12.
  * debian/control:
    - Add python3-setuptools and libpq-dev to build dependencies.
    - Add Testsuite.
    - Change Homepage to its new location.
    - Change maintainer email address.
    - Change Vcs to Salsa.
    - Reformat long description.
    - Replace libjs dependencies with sphinxdoc.
    - Update debhelper to 12.
    - Update Standards-Version to 4.4.0.
    - Update X-Python3-Version to >= 3.6.
  * debian/copyright:
    - Add and adjust all debian authors.
    - Change BSD-3 to BSD-3-Clause.
    - Change Evans-fcrypt license to MIT-Modified.
    - Remove paragraph for postgresql/test/test_dbapi20.py file, since its
      not necessary and same license as the upstream.
    - Replace BSD-3-Clause license text with the standard format.
    - Update copyright format for postgresql/resolved/crypt.py file.
    - Update copyright years for upstream.
    - Update Source URL.
    - Use https Format URL.
  * debian/doc-base: rename from python3-postgresql.doc-base
  * debian/docs: remove now unused build/docs/html entry.
  * debian/install: install libsys.sql into /usr/share/python3-postgresql
  * debian/links: remove in favor of sphinxdoc.
  * debian/patches:
    - Add 03-fix_pgsql_vers.patch to fix PostgreSQL version with debian suffix.
      (Closes: #890646)
    - Remove use_python3_for_sphinx.patch in favor of sphinxdoc build change.
    - Rename patches to include ordering number.
    - Change 02-use_stock_crypt.patch to include header.
  * debian/README.Debian: explain why git version is being used.
  * debian/rules:
    - Add hardening support.
    - Simplify using pybuild.
    - Use sphinxdoc.
  * debian/source/options: add to ignore egg-info.
  * debian/watch: update to version 4 and change it for github.

 -- William Grzybowski <william@grzy.org>  Fri, 12 Jul 2019 12:08:35 -0300

py-postgresql (1.1.0-2) unstable; urgency=medium

  * Team upload.
  * Drop unneeded depends on python3-ipaddr and adjust X-Python3-Version to
    account for use of the ipaddress module found in python3.3 and later
    (Closes: #745235)

 -- Scott Kitterman <scott@kitterman.com>  Sat, 17 May 2014 01:41:23 -0400

py-postgresql (1.1.0-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Daniel Kahn Gillmor ]
  * New upstream release (Closes: #736447, #724597)
  * update debian/watch (Closes: #736450)
  * bump debhelper to 9
  * Add dependency on python3-ipaddr
  * bump Standards-Version to 3.9.5 (no changes needed)
  * refreshed add_libpath.patch
  * added myself to Uploaders as part of the python-modules-team
  * replaced resolved/crypt with crypt from libpython3-stdlib to reduce
    licensing silliness

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 23 Jan 2014 13:44:16 -0500

py-postgresql (1.0.2-1) unstable; urgency=low

  * Initial release (Closes: #591048)

 -- William Grzybowski <william@agencialivre.com.br>  Sat, 31 Jul 2010 10:47:56 +0000
